WHat is a Data Model?

    A data model describes how data is organized and grouped in a database

    By creating data models, we can anticipate which data will be stored and managed in our database by our chosen DBMS in accordance to the application to be developed

Data modeling 
    Database should have a purpose and its organization must be related to the kind of application we are building

When we are creating an application, one of the first steps in its creating is determining the data models

Scenario:
    A Course Booking System Application wherein a user can book into a Course

    type: Course Booking System
    Description: A course booking system application where a user can book into a course.

    Features:
        _ User Registration
        - User Login/Authentication
            - Authenticated Users:
                - View course
                - User enrollment
                - Cancel enrollment
                - Update Details (with admin user verification)
                - Remove Details (with admin user verification)

            - Admin User
                - Add course
                - View/Read Courses
                - Update Course
                - Archive/Deactivate Course
                - ReActivate Course

            -All Users (guests, authenticated,admin)
                - View Active Courses
    Data Model

        User Document, Course Document, Transaction/Enrollment Document

        {
            username: "TJ",
            password: "1234"
        },
        {
            email: "romenick@gmailcom",
            password: "1235"
        }

        - Data Model are blueprints for our documents in our database
        - Data models are created so that we can follow and structure our data consistently

        User Data Model
            User {
                id = unique identifier for the document,
                username,
                firstName,
                lastName,
                email,
                password,
                mobileNumber,
                isAdmin,
            }

            Course {
                id = unique identifier for the document,
                Description,
                name,
                unit,
                slots,
                schedule,
                price,
                isActive,
                instructor
            }

            Enrollment {
                id = unique identifier for the document,
                userID - the unique identifier for the enrolled user,
                username - optional,
                courseID - the unique identifier for the course,
                courseName - optional,
                dateEnrolled,
                modeOfPayment,
                status
            }

        Model Relationship

           To be able to properly orgainze an application database we should be able to identify the relationships between our models. So that we can establish a relationship between our documents.

           One to One - This relationship means that a mode is excusively related to only one model
           
           Employee:
           {
                "id": "2023Dev",
                "firstName": "Jack",
                "lastName": "Sullivan",
                "email": "jsDev2023Dev@gmail.com"
           }
           Credentials:
           {
                "id": "creds_01",
                "employee_id": "2023Dev",
                "role": "developer",
                "team": "tech"
           }

           IN MongoDB, one to one relationship can be expressed in another way instead of referencing

                Embedding - embed is to oput another docunent in a document
                    SubDocument - is a document embedded/insided another docunent

                    Employee:
                    {
                            "id": "2023Dev",
                            "firstName": "Jack",
                            "lastName": "Sullivan",
                            "email": "jsDev2023Dev@gmail.com",
                            "credentials": {
                                "id": "creds_01",
                                "role": "developer",
                                "team": "tech"
                            }
                    }
            One to Many

                One model is related to multiple other models.
                However, the other models are only related to one

                Person - Many Email Address

                Email Address - One Person

                Blog Post -> comments 

                    A blog post can have many comments but each comment should only refer to that single blog post

                Blog: {
                    "id": "blog1-23",
                    "title": "This is an Awesome Blog",
                    "content": "This is an awesome blog that i created",
                    "createdOn": "11/1/2023",
                    "author": "BlogWriter1"
                }

                Comments :

                    {
                        "id": "blogcomment1",
                        "comment": "Awesome Blog",
                        "author": "blogGuy1",
                        "blog_id": "blog1-23"
                        
                    },
                    {
                        "id": "blogcomment2",
                        "comment": "Meh, Not Awesome at all",
                        "author": "notHater22",
                        "blog_id": "blog1-23"
                        
                    }

                In MongoDB, one to many relationship can also be expressed in another way:

                    SubDocument Array - an array of SubDocuments per single parent document
                 
                 Blog: {
                    "id": "blog1-23",
                    "title": "This is an Awesome Blog",
                    "content": "This is an awesome blog that i created",
                    "createdOn": "11/1/2023",
                    "author": "BlogWriter1",
                    "comments": [
                        {
                            "id": "blogcomment1",
                            "comment": "Awesome Blog",
                            "author": "blogGuy1"
                        },
                        {
                            "id": "blogcomment2",
                            "comment": "Meh, Not Awesome at all",
                            "author": "notHater22"
                        }
                    ]
                }

            Many to Many
                Multiple documents are related to multiple documents

                users - courses

                courses - users

                When a many to many relationshiop is create, for models to relate each other, an associative entity is created.

                Associative Entity is a model that relates models in the many to many relationship

                user - enrollment - course

                So that a user can relate to a crouse and sothat we can track the enrollment of a user to acourse, we have to create the details of the enrollment 

                User {
                    "id": "student1",
                    "firstName": "Ryan",
                    "lastName": "Yumul",
                    "email": "ryan.yumul@gmail.com",
                    "password": "ryan1234",
                    "mobileNumber": "09266772111",
                    "isAdmin": false,
                }

                Course {
                    "id": "course1",
                    "name": "Python-Django",
                    "description": "Learn the famous Python framework, Django",
                    "price": 25000,
                    "isActive": true
                }

                Enrollment {
                    "id": "enrollment1",
                    "userID": "student1",
                    "courseId": "course1",
                    "courseName": "Python-Django",
                    "dateEnrolled": "11/1/2023"
                }

                In MongoDB, many to many relationships can also be expressed in antother way:

                    Two-Way Embedding - In two-way embedding the associative entity is created and embedded in both models/documents

                    User {
                        "id": "student1",
                        "firstName": "Ryan",
                        "lastName": "Yumul",
                        "email": "ryan.yumul@gmail.com",
                        "password": "ryan1234",
                        "mobileNumber": "09266772111",
                        "isAdmin": false,
                        "enrollments": [
                             {
                                "id": "enrollment1",
                                "courseId": "course1",
                                "courseName": "Python-Django",
                                "dateEnrolled": "11/1/2023"
                             }
                           ]
                        }

                    Course {
                        "id": "course1",
                        "name": "Python-Django",
                        "description": "Learn the famous Python framework, Django",
                        "price": 25000,
                        "isActive": true,
                        "enrolles": [
                            {
                                "id": "enrollee1",
                                "userID": "student1",
                                "dateEnrolled": "11/1/2023"
                            } 
                        ]
                    }

Mini-Activity

    Create a sample document for the following Data Model:

    Product {
        id,
        name,
        description,
        price,
        isActive
    }
    User {
        id,
        email,
        password,
        mobileNumber,
        isAdmin
    }

    //Create a sample product Document
    //Create a sample User document

    Product {
        "id": "laptop-01",
        "name": "Acer",
        "description": "Helios-300",
        "price": "79999",
        "isActive": true
    }

     User {
        "id": "user-01",
        "email": "spongebob24@gmail.com",
        "password": "spongebobSquare",
        "mobileNumber": 0912345678,
        "isAdmin": false
    }




Translating data models into an ERD

    It aims to show what attributes (fields) an entity (collection) has. It also shows the relationship of attributes between entities.

    Entity Relationship Diagrams are commonly used for SQL and relational databases. 
    Since NoSQL is a non-relational database meaning there are no joining tables that connect multiple tables together, they would normally have arrays with a list of unique ids of other objects that connect them. 
    ERDs are still commonly used by developers to create a visual representation of how data is structured.